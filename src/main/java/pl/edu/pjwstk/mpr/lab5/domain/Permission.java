package pl.edu.pjwstk.mpr.lab5.domain;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class Permission {
    private String name;

    public String getName() {
        return name;
    }

    public Permission setName(String name) {
        this.name = name;
        return this;
    }
}
