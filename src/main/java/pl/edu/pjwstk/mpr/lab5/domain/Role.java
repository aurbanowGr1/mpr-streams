package pl.edu.pjwstk.mpr.lab5.domain;

import java.util.List;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class Role {

    private String name;
    List<Permission> permissions;

    public String getName() {
        return name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public Role setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
        return this;
    }
}
